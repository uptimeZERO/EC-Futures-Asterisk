﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Asterisk
{
    public partial class Asterisk_EditStudent : Form
    {
        public Asterisk_EditStudent()
        {
            InitializeComponent();
        }
        DBConnect MySQL = new DBConnect();
        private void Asterisk_EditStudent_Load(object sender, EventArgs e)
        {
            try
            {
                string SelectQuery = "SELECT * FROM faculty; ";
                MySQL.tempDS1(SelectQuery);
                faculty_cmb.DisplayMember = "name";
                faculty_cmb.ValueMember = "f_id";
                faculty_cmb.DataSource = MySQL.dt1;
                SelectQuery = "SELECT * FROM degree; ";
                MySQL.tempDS2(SelectQuery);
                course_cmb.DisplayMember = "name";
                course_cmb.ValueMember = "d_id";
                course_cmb.DataSource = MySQL.dt2;
            }
            catch
            {
                MessageBox.Show("An error has occured, please check your connection", "EC Futures");
            }
            studentid_txt.Text = EditTempStudent.s_id;
            forename_txt.Text = EditTempStudent.forename;
            surname_txt.Text = EditTempStudent.surname;
            faculty_cmb.SelectedValue = EditTempStudent.f_id;
            course_cmb.SelectedValue = EditTempStudent.d_id;
            email_txt.Text = EditTempStudent.email;
            phone_txt.Text = EditTempStudent.mobile;
            notes_txt.Text = EditTempStudent.notes;
            GetAppsData();
            GetRolesData();
        }
        public void GetAppsData()
        {
            try
            {
                string MassSelectionQuery = "SELECT a.a_id AS 'App ID', DATE_FORMAT(a.date, '%d/%m/%Y') AS Date, a.time AS Time, CONCAT(u.forename, ' ', u.surname) AS 'Staff Member', GROUP_CONCAT(ar.reason SEPARATOR ', ') AS Reason FROM appointment a, user u, student s, appointment_reason ar, appointment_details ad WHERE u.u_id = a.u_id AND s.s_id = a.s_id AND a.a_id = ad.a_id AND ar.ar_id = ad.ar_id AND s.s_id=" + EditTempStudent.s_id + " GROUP BY a.a_id;"; 
                MySQL.FetchAppointments(MassSelectionQuery);
                a_dgv.DataSource = MySQL.a_DS.Tables[0];
            }
            catch
            {
                MessageBox.Show("An error has occured, please check your connection", "EC Futures");
            }
        }
        public void GetRolesData()
        {
            try
            {
                string MassSelectionQuery2 = "SELECT rs.rs_id AS 'Role ID', rs.status AS Status, r.title AS 'Job Title', e.name AS Employer, r.deadline AS Deadline, r.start_date AS 'Start Date', r.start_date AS 'End Date' FROM role r, role_status rs, student s, employer e WHERE r.r_id = rs.r_id AND s.s_id = rs.s_id And r.e_id = e.e_id AND s.s_id=" + EditTempStudent.s_id + "";
                MySQL.FetchRoles2(MassSelectionQuery2);
                r_dgv.DataSource = MySQL.rr_DS.Tables[0];
            }
            catch
            {
                MessageBox.Show("An error has occured, please check your connection", "EC Futures");
            }
        }
        private void add_btn_Click(object sender, EventArgs e)
        {
            if (studentid_txt.Text == "" || forename_txt.Text == "" || surname_txt.Text == "" || faculty_cmb.Text == "" || course_cmb.Text == "" || email_txt.Text == "" || phone_txt.Text == "")
            {
                MessageBox.Show("Please make sure you have entered the appropriate data in the required(*) fields", "EC Futures");
            }
            else
            {
                try
                {
                    string f_str = faculty_cmb.SelectedValue.ToString();
                    int f_id;
                    bool f_parsed = Int32.TryParse(f_str, out f_id);
                    string d_str = course_cmb.SelectedValue.ToString();
                    int d_id;
                    bool d_parsed = Int32.TryParse(d_str, out d_id);
                    string query = "UPDATE student SET s_id=" + studentid_txt.Text + ", forename='" + forename_txt.Text + "', surname='" + surname_txt.Text + "', f_id=" + f_id + ", d_id=" + d_id + ", email='" + email_txt.Text + "', mobile='" + phone_txt.Text + "', cv='" + cv_stat_txt.Text + "', notes='" + notes_txt.Text + "' WHERE s_id=" + EditTempStudent.s_id + ";";
                    MySQL.PerformQuery(query);
                    MySQL.studREF(1);
                    this.Hide();
                }
                catch
                {
                    MessageBox.Show("An error has occured, please check your connection", "EC Futures");
                }
            }
        }
        private void cancel_btn_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void add_btn_Click_1(object sender, EventArgs e)
        {
            if (studentid_txt.Text == "" || forename_txt.Text == "" || surname_txt.Text == "" || faculty_cmb.Text == "" || course_cmb.Text == "" || email_txt.Text == "" || phone_txt.Text == "")
            {
                MessageBox.Show("Please make sure you have entered the appropriate data in the required(*) fields", "EC Futures");
            }
            else
            {
                try
                {
                    string f_str = faculty_cmb.SelectedValue.ToString();
                    int f_id;
                    bool f_parsed = Int32.TryParse(f_str, out f_id);
                    string d_str = course_cmb.SelectedValue.ToString();
                    int d_id;
                    bool d_parsed = Int32.TryParse(d_str, out d_id);
                    string query = "UPDATE student SET s_id=" + studentid_txt.Text + ", forename='" + forename_txt.Text + "', surname='" + surname_txt.Text + "', f_id=" + f_id + ", d_id=" + d_id + ", email='" + email_txt.Text + "', mobile='" + phone_txt.Text + "', cv='" + cv_stat_txt.Text + "', notes='" + notes_txt.Text + "' WHERE s_id=" + EditTempStudent.s_id + ";";
                    MySQL.PerformQuery(query);
                    MySQL.studREF(1);
                    this.Hide();
                }
                catch
                {
                    MessageBox.Show("An error has occured, please check your connection", "EC Futures");
                }
            }
        }

        private void a_del_btn_Click(object sender, EventArgs e)
        {
            var confirmDelete = MessageBox.Show("Are you sure to delete this item?", "EC Futures", MessageBoxButtons.YesNo);
            if (confirmDelete == DialogResult.Yes)
            {
                try
                {
                    int i = 0;
                    string firstCellValue = a_dgv.SelectedRows[0].Cells[0].Value.ToString();
                    i = int.Parse(firstCellValue);
                    i = Convert.ToInt32(firstCellValue);
                    string SelectQuery = "DELETE FROM appointment WHERE a_id = " + firstCellValue + "; ";
                    MySQL.PerformQuery(SelectQuery);
                    if (a_dgv.CurrentCell.RowIndex == 0)
                    {
                        MySQL.AppIND(0);
                    }
                    else
                    {
                        MySQL.AppIND(a_dgv.CurrentCell.RowIndex - 1);
                    }
                    MySQL.AppsREF(1);
                }
                catch
                {
                    MessageBox.Show("An error has occured, please check your connection", "EC Futures");
                }
            }
        }

        private void Appointment_ref_Tick(object sender, EventArgs e)
        {
            if (DGV_Refresh.Appointment == 1)
            {
                GetAppsData();
                try
                {
                    a_dgv.CurrentCell = a_dgv.Rows[DGV_Refresh.iAppointment].Cells[0];
                }
                catch
                {
                    
                }
                MySQL.AppsREF(0);
            }
        }

        private void Roles_ref_Tick(object sender, EventArgs e)
        {
            if (DGV_Refresh.Roles2 == 1)
            {
                GetRolesData();
                try
                {
                    r_dgv.CurrentCell = r_dgv.Rows[DGV_Refresh.iRoles2].Cells[0];
                    r_dgv.Columns[1].DefaultCellStyle.Format = "dd/MM/yyyy";
                }
                catch
                {

                }
                MySQL.Roles2REF(0);
            }
        }

        private void r_del_btn_Click(object sender, EventArgs e)
        {
            var confirmDelete = MessageBox.Show("Are you sure to delete this item?", "EC Futures", MessageBoxButtons.YesNo);
            if (confirmDelete == DialogResult.Yes)
            {
                try
                {
                    int i = 0;
                    string firstCellValue = r_dgv.SelectedRows[0].Cells[0].Value.ToString();
                    i = int.Parse(firstCellValue);
                    i = Convert.ToInt32(firstCellValue);
                    string SelectQuery = "DELETE FROM role_status WHERE rs_id = " + firstCellValue + "; ";
                    MySQL.PerformQuery(SelectQuery);
                    if (r_dgv.CurrentCell.RowIndex == 0)
                    {
                        MySQL.Role2IND(0);
                    }
                    else
                    {
                        MySQL.Role2IND(r_dgv.CurrentCell.RowIndex - 1);
                    }
                    MySQL.Roles2REF(1);
                }
                catch
                {
                    MessageBox.Show("An error has occured, please check your connection", "EC Futures");
                }
            }
        }

        private void a_add_btn_Click(object sender, EventArgs e)
        {
            MySQL.AppIND(a_dgv.Rows.Count);
            Asterisk_AddAppointment frm = new Asterisk_AddAppointment();
            frm.Show();
        }

        private void cancel_btn_Click_1(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void a_edit_btn_Click(object sender, EventArgs e)
        {
            MySQL.AppIND(a_dgv.CurrentCell.RowIndex);
            string firstCellValue = a_dgv.SelectedRows[0].Cells[0].Value.ToString();
            string query = "SELECT a_id, s_id, DATE_FORMAT(date, '%d/%m/%Y') as date, time, u_id, action_points, adviser_comments FROM appointment WHERE a_id=" + firstCellValue + "";
            MySQL.tempAppStore(query);
            Asterisk_EditAppointment frm = new Asterisk_EditAppointment();
            frm.Show();
        }
    }
}
