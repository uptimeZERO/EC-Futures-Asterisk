﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Asterisk
{
    public partial class Asterisk_EditEmployer : Form
    {
        public Asterisk_EditEmployer()
        {
            InitializeComponent();
        }
        DBConnect MySQL = new DBConnect();
        private void add_btn_Click(object sender, EventArgs e)
        {
            if (employer_txt.Text == "" || address1_txt.Text == "" || settlement_txt.Text == "" || county_txt.Text == "" || postcode_txt.Text == "" || country_cmb.Text == "")
            {
                MessageBox.Show("Please make sure you have entered the appropriate data in the required(*) fields", "EC Futures");
            }
            else
            {
                try
                {
                    string query = "UPDATE employer SET name='" + employer_txt.Text + "', address_1='" + address1_txt.Text + "', address_2='" + address2_txt.Text + "', settlement='" + settlement_txt.Text + "', county='" + county_txt.Text + "', post_code='" + postcode_txt.Text + "', country='" + country_cmb.Text + "', notes='" + notes_txt.Text + "' WHERE e_id=" + EditTempEmployer.e_id + ";";
                    MySQL.PerformQuery(query);
                    MySQL.EmpREF(1);
                    this.Hide();
                }
                catch
                {
                    MessageBox.Show("An error has occured, please check your connection", "EC Futures");
                }
            }
        }

        private void cancel_btn_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void Asterisk_EditEmployer_Load(object sender, EventArgs e)
        {
            employer_txt.Text = EditTempEmployer.name;
            address1_txt.Text = EditTempEmployer.address_1;
            address2_txt.Text = EditTempEmployer.address_2;
            settlement_txt.Text = EditTempEmployer.settlement;
            county_txt.Text = EditTempEmployer.county;
            postcode_txt.Text = EditTempEmployer.post_code;
            country_cmb.Text = EditTempEmployer.country;
            notes_txt.Text = EditTempEmployer.notes;
        }
    }
}
