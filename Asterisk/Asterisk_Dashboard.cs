﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Asterisk
{
    public partial class Asterisk_Dashboard : Form
    {
        public Asterisk_Dashboard()
        {
            InitializeComponent();
        }
        DBConnect MySQL = new DBConnect();
        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime now = DateTime.Now;
            label1.Text = (now.ToString());
        }

        private void toolStripDropDownButton1_Click(object sender, EventArgs e)
        {

        }

        private void toolStripDropDownButton1_Click_1(object sender, EventArgs e)
        {

        }

        private void File_Exit_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void Change_user_tool_Click(object sender, EventArgs e)
        {
            this.Hide();
            Asterisk_login frm = new Asterisk_login();
            frm.Show();
        }

        private void Create_New_user_tool_Click(object sender, EventArgs e)
        {
            CreateUser();
        }

        private void Asterisk_Dashboard_Load(object sender, EventArgs e)
        {
            pullAll();
        }
        private void CreateUser()
        {
            if (userGroup.userGroupID == "1")
            {
                MessageBox.Show("Insufficient permissions, please contact an administrator", "EC Futures");
            }
            else
            {
                Asterisk_CreateNewUser frm = new Asterisk_CreateNewUser();
                frm.Show();
            }
        }
        private void ManageUsers()
        {
            if (userGroup.userGroupID == "1")
            {
                MessageBox.Show("Insufficient permissions, please contact an administrator", "EC Futures");
            }
            else
            {
                MessageBox.Show("This feature is not available yet", "EC Futures");
            }
        }

        private void manageUsersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ManageUsers();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GetRoleData();
        }

        private void vt_fetch_btn_Click(object sender, EventArgs e)
        {
            GetVisitingTutorData();
        }

        private void ec_fetch_btn_Click(object sender, EventArgs e)
        {
            GetEmployerContactData();
        }

        private void e_fetch_btn_Click(object sender, EventArgs e)
        {
            GetEmployerData();
        }

        private void s_fetch_btn_Click(object sender, EventArgs e)
        {
            GetStudentData();
        }
        public void pullAll()
        {
            try
            {
                GetRoleData();
                GetStudentData();
                GetEmployerData();
                GetEmployerContactData();
                GetVisitingTutorData();
            }
            catch
            {
                MessageBox.Show("An error has occured, please check your connection", "EC Futures");
            }
        }
        // ************** WORK IN PROGRESS DO NOT TOUCH ************** //
        private void s_delete_btn_Click(object sender, EventArgs e)
        {
            var confirmDelete = MessageBox.Show("Are you sure to delete this item?", "EC Futures", MessageBoxButtons.YesNo);
            if (confirmDelete == DialogResult.Yes)
            {
                try
                {
                    int i = 0;
                    string firstCellValue = s_dgv.SelectedRows[0].Cells[0].Value.ToString();
                    i = int.Parse(firstCellValue);
                    i = Convert.ToInt32(firstCellValue);
                    string SelectQuery = "DELETE FROM student WHERE s_id = " + firstCellValue + "; ";
                    MySQL.PerformQuery(SelectQuery);
                    if (s_dgv.CurrentCell.RowIndex == 0)
                    {
                        MySQL.studIND(0);
                    }
                    else
                    {
                        MySQL.studIND(s_dgv.CurrentCell.RowIndex - 1);
                    }
                    MySQL.studREF(1);

                }
                catch
                {
                    MessageBox.Show("An error has occured, please check your connection", "EC Futures");
                }
            }
        }
        public void GetStudentData()
        {
            try
            {
                string MassSelectionQuery = "SELECT s.s_id as 'Student ID', s.forename as Forename, s.surname as Surname, f.name as Faculty, d.name as Degree, s.email as Email, s.mobile as Mobile, s.cv as CV, s.notes as Notes FROM student s, faculty f, degree d WHERE s.f_id = f.f_id AND s.d_id = d.d_id";
                MySQL.FetchStudent(MassSelectionQuery);
                s_dgv.DataSource = MySQL.s_DS.Tables[0];
            }
            catch
            {
                MessageBox.Show("An error has occured, please check your connection", "EC Futures");
            }
        }
        public void GetStudentDataMod(string MassSelectionQuery)
        {
            try
            {
                MySQL.FetchStudent(MassSelectionQuery);
                s_dgv.DataSource = MySQL.s_DS.Tables[0];
            }
            catch
            {
                MessageBox.Show("An error has occured, please check your connection", "EC Futures");
            }
        }
        public void GetEmployerData()
        {
            try
            {
                string MassSelectionQuery = "SELECT e_id as 'Employer ID', name as 'Employer', address_1 as 'Address 1', address_2 as 'Address 2', settlement as Settlement, county as County, post_code as 'Post Code', country as Country, notes as Notes FROM employer;";
                MySQL.FetchEmployer(MassSelectionQuery);
                e_dgv.DataSource = MySQL.e_DS.Tables[0];
            }
            catch
            {
                MessageBox.Show("An error has occured, please check your connection", "EC Futures");
            }
        }
        public void GetEmployerDataMod(string MassSelectionQuery)
        {
            try
            {
                MySQL.FetchEmployer(MassSelectionQuery);
                e_dgv.DataSource = MySQL.e_DS.Tables[0];
            }
            catch
            {
                MessageBox.Show("An error has occured, please check your connection", "EC Futures");
            }
        }
        public void GetVisitingTutorData()
        {
            try
            {
                string MassSelectionQuery = "SELECT vt_id as 'Visting Tutor ID', forename as Forename, surname as Surename, email as Email, notes as Notes FROM visiting_tutor;";
                MySQL.FetchVisitingTutor(MassSelectionQuery);
                vt_dgv.DataSource = MySQL.vt_DS.Tables[0];
            }
            catch
            {
                MessageBox.Show("An error has occured, please check your connection", "EC Futures");
            }
        }
        public void GetVisitingTutorDataMod(string MassSelectionQuery)
        {
            try
            {
                MySQL.FetchVisitingTutor(MassSelectionQuery);
                vt_dgv.DataSource = MySQL.vt_DS.Tables[0];
            }
            catch
            {
                MessageBox.Show("An error has occured, please check your connection", "EC Futures");
            }
        }
        public void GetEmployerContactData()
        {
            try
            {
                string MassSelectionQuery = "SELECT c.c_id as 'Contact ID', e.name as 'Employer', c.forename as Forename, c.surname as Surname, c.email as Email, c.phone as Phone, c.mobile as Mobile, c.notes as Notes FROM contact c, employer e WHERE e.e_id = c.e_id";
                MySQL.FetchContact(MassSelectionQuery);
                ec_dgv.DataSource = MySQL.ec_DS.Tables[0];
            }
            catch
            {
                MessageBox.Show("An error has occured, please check your connection", "EC Futures");
            }
        }
        public void GetEmployerContactDataMod(string MassSelectionQuery)
        {
            try
            {
                MySQL.FetchContact(MassSelectionQuery);
                ec_dgv.DataSource = MySQL.ec_DS.Tables[0];
            }
            catch
            {
                MessageBox.Show("An error has occured, please check your connection", "EC Futures");
            }
        }
        public void GetRoleData()
        {
            try
            {
                string MassSelectionQuery = "SELECT r.r_id as 'Roles ID', r.title as 'Job Role', r.salary as Salary, e.name as Employer, c.forename as Forename, c.surname as Surname, DATE_FORMAT(r.deadline, '%d/%m/%Y') as Deadline, DATE_FORMAT(r.start_date, '%d/%m/%Y') as 'Start Date', DATE_FORMAT(r.end_date, '%d/%m/%Y') as 'End Date', r.notes as Notes FROM role r, contact c, employer e WHERE r.e_id = e.e_id AND r.c_id = c.c_id";
                MySQL.FetchRole(MassSelectionQuery);
                r_dgv.DataSource = MySQL.r_DS.Tables[0];
            }
            catch
            {
                MessageBox.Show("An error has occured, please check your connection", "EC Futures");
            }
        }
        public void GetRoleDataMod(string MassSelectionQuery)
        {
            try
            {
                MySQL.FetchRole(MassSelectionQuery);
                r_dgv.DataSource = MySQL.r_DS.Tables[0];
            }
            catch
            {
                MessageBox.Show("An error has occured, please check your connection", "EC Futures");
            }
        }

        private void e_delete_btn_Click(object sender, EventArgs e)
        {
            var confirmDelete = MessageBox.Show("Are you sure to delete this item?", "EC Futures", MessageBoxButtons.YesNo);
            if (confirmDelete == DialogResult.Yes)
            {
                try
                {
                    int i = 0;
                    string firstCellValue = e_dgv.SelectedRows[0].Cells[0].Value.ToString();
                    i = int.Parse(firstCellValue);
                    i = Convert.ToInt32(firstCellValue);
                    string SelectQuery = "DELETE FROM employer WHERE e_id = " + firstCellValue + "; ";
                    MySQL.PerformQuery(SelectQuery);
                    if (e_dgv.CurrentCell.RowIndex == 0)
                    {
                        MySQL.EmpIND(0);
                    }
                    else
                    {
                        MySQL.EmpIND(e_dgv.CurrentCell.RowIndex - 1);
                    }
                    MySQL.EmpREF(1);

                }
                catch
                {
                    MessageBox.Show("An error has occured, please check your connection", "EC Futures");
                }
            }
        }

        private void ec_delete_btn_Click(object sender, EventArgs e)
        {
            var confirmDelete = MessageBox.Show("Are you sure to delete this item?", "Asterisk", MessageBoxButtons.YesNo);
            if (confirmDelete == DialogResult.Yes)
            {
                try
                {
                    int i = 0;
                    string firstCellValue = ec_dgv.SelectedRows[0].Cells[0].Value.ToString();
                    i = int.Parse(firstCellValue);
                    i = Convert.ToInt32(firstCellValue);
                    string SelectQuery = "DELETE FROM contact WHERE c_id = " + firstCellValue + "; ";
                    MySQL.PerformQuery(SelectQuery);
                    if (ec_dgv.CurrentCell.RowIndex == 0)
                    {
                        MySQL.ContIND(0);
                    }
                    else
                    {
                        MySQL.ContIND(ec_dgv.CurrentCell.RowIndex - 1);
                    }
                    MySQL.ContREF(1);

                }
                catch
                {
                    MessageBox.Show("You have selected a cell, you must select a row", "EC Futures");
                }
            }
        }

        private void r_delete_btn_Click(object sender, EventArgs e)
        {
            var confirmDelete = MessageBox.Show("Are you sure to delete this item?", "EC Futures", MessageBoxButtons.YesNo);
            if (confirmDelete == DialogResult.Yes)
            {
                try
                {
                    int i = 0;
                    string firstCellValue = r_dgv.SelectedRows[0].Cells[0].Value.ToString();
                    i = int.Parse(firstCellValue);
                    i = Convert.ToInt32(firstCellValue);
                    string SelectQuery = "DELETE FROM role WHERE r_id = " + firstCellValue + "; ";
                    MySQL.PerformQuery(SelectQuery);
                    if (r_dgv.CurrentCell.RowIndex == 0)
                    {
                        MySQL.RoleIND(0);
                    }
                    else
                    {
                        MySQL.RoleIND(r_dgv.CurrentCell.RowIndex - 1);
                    }
                    MySQL.RoleREF(1);

                }
                catch
                {
                    MessageBox.Show("You have selected a cell, you must select a row", "EC Futures");
                }
            }
        }
        private void vt_delete_btn_Click(object sender, EventArgs e)
        {
            var confirmDelete = MessageBox.Show("Are you sure to delete this item?", "Asterisk", MessageBoxButtons.YesNo);
            if (confirmDelete == DialogResult.Yes)
            {
                try
                {
                    int i = 0;
                    string firstCellValue = vt_dgv.SelectedRows[0].Cells[0].Value.ToString();
                    i = int.Parse(firstCellValue);
                    i = Convert.ToInt32(firstCellValue);
                    string SelectQuery = "DELETE FROM visiting_tutor WHERE vt_id = " + firstCellValue + "; ";
                    MySQL.PerformQuery(SelectQuery);
                    if (vt_dgv.CurrentCell.RowIndex == 0)
                    {
                        MySQL.VisiIND(0);
                    }
                    else
                    {
                        MySQL.VisiIND(vt_dgv.CurrentCell.RowIndex - 1);
                    }
                    MySQL.VisiREF(1);

                }
                catch
                {
                    MessageBox.Show("An error has occured, please check your connection", "EC Futures");
                }
            }
        }


        private void StudentRefresh_Tick(object sender, EventArgs e)
        {
            if (DGV_Refresh.Student == 1)
            {
                GetStudentData();
                s_dgv.CurrentCell = s_dgv.Rows[DGV_Refresh.iStudent].Cells[0];
                MySQL.studREF(0);
            }
        }
        private void tab1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void r_add_btn_Click(object sender, EventArgs e)
        {
           
        }

        private void r_add_btn_Click_1(object sender, EventArgs e)
        {
            MySQL.RoleIND(r_dgv.Rows.Count);
            Asterisk_AddRoles frm = new Asterisk_AddRoles();
            frm.Show();
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string MassSelectionQuery = "SELECT s.s_id as 'Student ID', s.forename as Forename, s.surname as Surname, f.name as Faculty, d.name as Degree, s.email as Email, s.mobile as Mobile, s.cv as CV, s.notes as Notes FROM student s, faculty f, degree d WHERE s.f_id = f.f_id AND s.d_id = d.d_id";
            if (comboBox1.Text == "Student ID")
            {
                MassSelectionQuery = MassSelectionQuery + " AND s.s_id LIKE '" + textBox1.Text + "%'";
                GetStudentDataMod(MassSelectionQuery);
            }

            else if (comboBox1.Text == "Forename")
            {
                MassSelectionQuery = MassSelectionQuery + " AND s.forename LIKE '" + textBox1.Text + "%'";
                GetStudentDataMod(MassSelectionQuery);
            }
            else if (comboBox1.Text == "Surname")
            {
                MassSelectionQuery = MassSelectionQuery + " AND s.surname LIKE '" + textBox1.Text + "%'";
                GetStudentDataMod(MassSelectionQuery);
            }
            else if (comboBox1.Text == "Mobile")
            {
                MassSelectionQuery = MassSelectionQuery + " AND s.mobile LIKE '" + textBox1.Text + "%'";
                GetStudentDataMod(MassSelectionQuery);
            }
            else if (comboBox1.Text == "Degree")
            {
                MassSelectionQuery = MassSelectionQuery + " AND d.name LIKE '" + textBox1.Text + "%'";
                GetStudentDataMod(MassSelectionQuery);
            }
            else if (comboBox1.Text == "Faculty")
            {
                MassSelectionQuery = MassSelectionQuery + " AND f.name LIKE '" + textBox1.Text + "%'";
                GetStudentDataMod(MassSelectionQuery);
            }
            else if (comboBox1.Text == "Email")
            {
                MassSelectionQuery = MassSelectionQuery + " AND s.email LIKE '" + textBox1.Text + "%'";
                GetStudentDataMod(MassSelectionQuery);
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            string MassSelectionQuery = "SELECT e_id as 'Employer ID', name as 'Employer', address_1 as 'Address 1', address_2 as 'Address 2', settlement as Settlement, county as County, post_code as 'Post Code', country as Country, notes as Notes FROM employer WHERE";
            if (comboBox2.Text == "Employer ID")
            {
                MassSelectionQuery = MassSelectionQuery + " e_id LIKE '" + textBox2.Text + "%'";
                GetEmployerDataMod(MassSelectionQuery);
            }
            else if (comboBox2.Text == "Employer")
            {
                MassSelectionQuery = MassSelectionQuery + " name LIKE '" + textBox2.Text + "%'";
                GetEmployerDataMod(MassSelectionQuery);
            }
            else if (comboBox2.Text == "Address 1")
            {
                MassSelectionQuery = MassSelectionQuery + " address_1 LIKE '" + textBox2.Text + "%'";
                GetEmployerDataMod(MassSelectionQuery);
            }
            else if (comboBox2.Text == "Address 2")
            {
                MassSelectionQuery = MassSelectionQuery + " address_2 LIKE '" + textBox2.Text + "%'";
                GetEmployerDataMod(MassSelectionQuery);
            }
            else if (comboBox2.Text == "Settlement")
            {
                MassSelectionQuery = MassSelectionQuery + " settlement LIKE '" + textBox2.Text + "%'";
                GetEmployerDataMod(MassSelectionQuery);
            }
            else if (comboBox2.Text == "County")
            {
                MassSelectionQuery = MassSelectionQuery + " county LIKE '" + textBox2.Text + "%'";
                GetEmployerDataMod(MassSelectionQuery);
            }
            else if (comboBox2.Text == "Post Code")
            {
                MassSelectionQuery = MassSelectionQuery + " post_code LIKE '" + textBox2.Text + "%'";
                GetEmployerDataMod(MassSelectionQuery);
            }
            else if (comboBox2.Text == "Country")
            {
                MassSelectionQuery = MassSelectionQuery + " country LIKE '" + textBox2.Text + "%'";
                GetEmployerDataMod(MassSelectionQuery);
            }
        }
        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            string MassSelectionQuery = "SELECT c.c_id as 'Contact ID', e.name as 'Employer', c.forename as Forename, c.surname as Surname, c.email as Email, c.phone as Phone, c.mobile as Mobile, c.notes as Notes FROM contact c, employer e WHERE e.e_id = c.e_id  AND";
            if (comboBox3.Text == "Contact ID")
            {
                MassSelectionQuery = MassSelectionQuery + " c.c_id LIKE '" + textBox3.Text + "%'";
                GetEmployerContactDataMod(MassSelectionQuery);
            }
            else if (comboBox3.Text == "Employer")
            {
                MassSelectionQuery = MassSelectionQuery + " e.name LIKE '" + textBox3.Text + "%'";
                GetEmployerContactDataMod(MassSelectionQuery);
            }
            else if (comboBox3.Text == "Forename")
            {
                MassSelectionQuery = MassSelectionQuery + " c.forename LIKE '" + textBox3.Text + "%'";
                GetEmployerContactDataMod(MassSelectionQuery);
            }
            else if (comboBox3.Text == "Surname")
            {
                MassSelectionQuery = MassSelectionQuery + " c.surname LIKE '" + textBox3.Text + "%'";
                GetEmployerContactDataMod(MassSelectionQuery);
            }
            else if (comboBox3.Text == "Email")
            {
                MassSelectionQuery = MassSelectionQuery + " c.email LIKE '" + textBox3.Text + "%'";
                GetEmployerContactDataMod(MassSelectionQuery);
            }
            else if (comboBox3.Text == "Phone")
            {
                MassSelectionQuery = MassSelectionQuery + " c.phone LIKE '" + textBox3.Text + "%'";
                GetEmployerContactDataMod(MassSelectionQuery);
            }
            else if (comboBox3.Text == "Mobile")
            {
                MassSelectionQuery = MassSelectionQuery + " c.mobile LIKE '" + textBox3.Text + "%'";
                GetEmployerContactDataMod(MassSelectionQuery);
            }
        }
        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            //string fuck_you = "SELECT r.r_id as 'Roles ID', r.title as 'Job Role', r.salary as Salary, e.name as Employer, c.forename as Forename, c.surname as Surname, DATE_FORMAT(r.deadline, '%d/%m/%Y') as Deadline, DATE_FORMAT(r.start_date, '%d/%m/%Y') as 'Start Date', DATE_FORMAT(r.end_date, '%d/%m/%Y') as 'End Date', r.notes as Notes FROM role r, contact c, employer e WHERE r.e_id = e.e_id AND r.c_id = c.c_id";
            string MassSelectionQuery = "SELECT r.r_id as 'Roles ID', r.title as 'Job Role', r.salary as Salary, e.name as Employer, c.forename as Forename, c.surname as Surname, DATE_FORMAT(r.deadline, '%d/%m/%Y') as Deadline, DATE_FORMAT(r.start_date, '%d/%m/%Y') as 'Start Date', DATE_FORMAT(r.end_date, '%d/%m/%Y') as 'End Date', r.notes as Notes FROM role r, contact c, employer e WHERE r.e_id = e.e_id AND r.c_id = c.c_id AND";
            if (comboBox4.Text == "Roles ID")
            {
                MassSelectionQuery = MassSelectionQuery + " r.r_id LIKE '" + textBox4.Text + "%'";
                GetRoleDataMod(MassSelectionQuery);
            }
            else if (comboBox4.Text == "Job Role")
            {
                MassSelectionQuery = MassSelectionQuery + " r.title LIKE '" + textBox4.Text + "%'";
                GetRoleDataMod(MassSelectionQuery);
            }
            else if (comboBox4.Text == "Salary")
            {
                MassSelectionQuery = MassSelectionQuery + " r.salary LIKE '" + textBox4.Text + "%'";
                GetRoleDataMod(MassSelectionQuery);
            }
            else if (comboBox4.Text == "Employer")
            {
                MassSelectionQuery = MassSelectionQuery + " e.name LIKE '" + textBox4.Text + "%'";
                GetRoleDataMod(MassSelectionQuery);
            }
            else if (comboBox4.Text == "Forename")
            {
                MassSelectionQuery = MassSelectionQuery + " c.forename LIKE '" + textBox4.Text + "%'";
                GetRoleDataMod(MassSelectionQuery);
            }
            else if (comboBox4.Text == "Surname")
            {
                MassSelectionQuery = MassSelectionQuery + " c.surname LIKE '" + textBox4.Text + "%'";
                GetRoleDataMod(MassSelectionQuery);
            }
            else if (comboBox4.Text == "Deadline")
            {
                MassSelectionQuery = MassSelectionQuery + " r.deadline LIKE '" + textBox4.Text + "%'";
                GetRoleDataMod(MassSelectionQuery);
            }
            else if (comboBox4.Text == "Start Date")
            {
                MassSelectionQuery = MassSelectionQuery + " r.start_date LIKE '" + textBox4.Text + "%'";
                GetRoleDataMod(MassSelectionQuery);
            }
            else if (comboBox4.Text == "End Date")
            {
                MassSelectionQuery = MassSelectionQuery + " r.end_date LIKE '" + textBox4.Text + "%'";
                GetRoleDataMod(MassSelectionQuery);
            }
        }
        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            string MassSelectionQuery = "SELECT vt_id as 'Visting Tutor ID', forename as Forename, surname as Surename, email as Email, notes as Notes FROM visiting_tutor WHERE";
            if (comboBox5.Text == "Visiting Tutor ID")
            {
                MassSelectionQuery = MassSelectionQuery + " vt_id LIKE '" + textBox5.Text + "%'";
                GetVisitingTutorDataMod(MassSelectionQuery);
            }
            else if (comboBox5.Text == "Forename")
            {
                MassSelectionQuery = MassSelectionQuery + " forename LIKE '" + textBox5.Text + "%'";
                GetVisitingTutorDataMod(MassSelectionQuery);
            }
            else if (comboBox5.Text == "Surname")
            {
                MassSelectionQuery = MassSelectionQuery + " surname LIKE '" + textBox5.Text + "%'";
                GetVisitingTutorDataMod(MassSelectionQuery);
            }
            else if (comboBox5.Text == "Email")
            {
                MassSelectionQuery = MassSelectionQuery + " email LIKE '" + textBox5.Text + "%'";
                GetVisitingTutorDataMod(MassSelectionQuery);
            }
        }

        private void s_add_btn_Click(object sender, EventArgs e)
        {
            MySQL.studIND(s_dgv.Rows.Count);
            Asterisk_AddStudent frm = new Asterisk_AddStudent();
            frm.Show();
        }

        private void e_add_btn_Click(object sender, EventArgs e)
        {
            MySQL.EmpIND(e_dgv.Rows.Count);
            Asterisk_AddEmployer frm = new Asterisk_AddEmployer();
            frm.Show();
        }

        private void ec_add_btn_Click(object sender, EventArgs e)
        {
            MySQL.ContIND(ec_dgv.Rows.Count);
            Asterisk_AddEmployerContacts frm = new Asterisk_AddEmployerContacts();
            frm.Show();
        }

        private void vt_add_btn_Click(object sender, EventArgs e)
        {
            MySQL.VisiIND(vt_dgv.Rows.Count);
            Asterisk_AddVisitingTutors frm = new Asterisk_AddVisitingTutors();
            frm.Show();
        }
        private void button16_Click(object sender, EventArgs e)
        {
            MySQL.studIND(s_dgv.CurrentCell.RowIndex);
            string firstCellValue = s_dgv.SelectedRows[0].Cells[0].Value.ToString();
            string query = "SELECT * FROM student WHERE s_id=" + firstCellValue + ";";
            MySQL.tempStudStore(query);
            Asterisk_EditStudent frm = new Asterisk_EditStudent();
            frm.Show();
        }

        private void Edit_stud(object sender, MouseEventArgs e)
        {
            MySQL.studIND(s_dgv.CurrentCell.RowIndex);
            string firstCellValue = s_dgv.SelectedRows[0].Cells[0].Value.ToString();
            string query = "SELECT * FROM student WHERE s_id=" + firstCellValue + ";";
            MySQL.tempStudStore(query);
            Asterisk_EditStudent frm = new Asterisk_EditStudent();
            frm.Show();
        }

        private void student_refresh(object sender, EventArgs e)
        {
            GetStudentData();
        }

        private void EmployerRefresh_Tick(object sender, EventArgs e)
        {
            if (DGV_Refresh.Employer == 1)
            {
                GetEmployerData();
                e_dgv.CurrentCell = e_dgv.Rows[DGV_Refresh.iEmployer].Cells[0];
                MySQL.EmpREF(0);
            }
        }

        private void ContactsRefresh_Tick(object sender, EventArgs e)
        {
            if (DGV_Refresh.Contact == 1)
            {
                GetEmployerContactData();
                ec_dgv.CurrentCell = ec_dgv.Rows[DGV_Refresh.iContact].Cells[0];
                MySQL.ContREF(0);
            }
        }

        private void RolesRefresh_Tick(object sender, EventArgs e)
        {
            if (DGV_Refresh.Roles == 1)
            {
                GetRoleData();
                r_dgv.CurrentCell = r_dgv.Rows[DGV_Refresh.iRoles].Cells[0];
                MySQL.RoleREF(0);
            }
        }

        private void VisitingRefresh_Tick(object sender, EventArgs e)
        {
            if (DGV_Refresh.Visit == 1)
            {
                GetVisitingTutorData();
                vt_dgv.CurrentCell = vt_dgv.Rows[DGV_Refresh.iVisit].Cells[0];
                MySQL.VisiREF(0);
            }
        }

        private void Employer_Refresh(object sender, EventArgs e)
        {
            GetEmployerData();
        }

        private void Contact_Refresh(object sender, EventArgs e)
        {
            GetEmployerContactData();
        }

        private void Role_Refresh(object sender, EventArgs e)
        {
            GetRoleData();
        }

        private void Visit_Refresh(object sender, EventArgs e)
        {
            GetVisitingTutorData();
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void e_edit_btn_Click(object sender, EventArgs e)
        {
            MySQL.EmpIND(e_dgv.CurrentCell.RowIndex);
            string firstCellValue = e_dgv.SelectedRows[0].Cells[0].Value.ToString();
            string query = "SELECT * FROM employer WHERE e_id=" + firstCellValue + ";";
            MySQL.tempEmpStore(query);
            Asterisk_EditEmployer frm = new Asterisk_EditEmployer();
            frm.Show();
        }

        private void Edit_Emp(object sender, EventArgs e)
        {
            MySQL.EmpIND(e_dgv.CurrentCell.RowIndex);
            string firstCellValue = e_dgv.SelectedRows[0].Cells[0].Value.ToString();
            string query = "SELECT * FROM employer WHERE e_id=" + firstCellValue + ";";
            MySQL.tempEmpStore(query);
            Asterisk_EditEmployer frm = new Asterisk_EditEmployer();
            frm.Show();
        }

        private void ec_edit_btn_Click(object sender, EventArgs e)
        {
            MySQL.ContIND(ec_dgv.CurrentCell.RowIndex);
            string firstCellValue = ec_dgv.SelectedRows[0].Cells[0].Value.ToString();
            string query = "SELECT * FROM contact WHERE c_id=" + firstCellValue + ";";
            MySQL.tempContStore(query);
            Asterisk_EditEmployerContacts frm = new Asterisk_EditEmployerContacts();
            frm.Show();
        }

        private void Edit_EmpCont(object sender, MouseEventArgs e)
        {
            MySQL.ContIND(ec_dgv.CurrentCell.RowIndex);
            string firstCellValue = ec_dgv.SelectedRows[0].Cells[0].Value.ToString();
            string query = "SELECT * FROM contact WHERE c_id=" + firstCellValue + ";";
            MySQL.tempContStore(query);
            Asterisk_EditEmployerContacts frm = new Asterisk_EditEmployerContacts();
            frm.Show();
        }

        private void r_edit_btn_Click(object sender, EventArgs e)
        {
            MySQL.RoleIND(r_dgv.CurrentCell.RowIndex);
            string firstCellValue = r_dgv.SelectedRows[0].Cells[0].Value.ToString();
            string query = "SELECT r_id, title, salary, e_id, c_id, DATE_FORMAT(deadline, '%d/%m/%Y') as deadline, DATE_FORMAT(start_date, '%d/%m/%Y') as 'start_date', DATE_FORMAT(end_date, '%d/%m/%Y') as 'end_date', notes FROM role WHERE r_id=" + firstCellValue + ";";
            MySQL.tempRoleStore(query);
            Asterisk_EditRoles frm = new Asterisk_EditRoles();
            frm.Show();
        }

        private void Edit_Role(object sender, MouseEventArgs e)
        {
            MySQL.RoleIND(r_dgv.CurrentCell.RowIndex);
            string firstCellValue = r_dgv.SelectedRows[0].Cells[0].Value.ToString();
            string query = "SELECT r_id, title, salary, e_id, c_id, DATE_FORMAT(deadline, '%d/%m/%Y') as deadline, DATE_FORMAT(start_date, '%d/%m/%Y') as 'start_date', DATE_FORMAT(end_date, '%d/%m/%Y') as 'end_date', notes FROM role WHERE r_id=" + firstCellValue + ";";
            MySQL.tempRoleStore(query);
            Asterisk_EditRoles frm = new Asterisk_EditRoles();
            frm.Show();
        }

        private void vt_edit_btn_Click(object sender, EventArgs e)
        {
            MySQL.VisiIND(vt_dgv.CurrentCell.RowIndex);
            string firstCellValue = vt_dgv.SelectedRows[0].Cells[0].Value.ToString();
            string query = "SELECT * FROM visiting_tutor WHERE vt_id=" + firstCellValue + ";";
            MySQL.tempVTStore(query);
            Asterisk_EditVisitingTutors frm = new Asterisk_EditVisitingTutors();
            frm.Show();
        }

        private void Edit_VisitingTutor(object sender, MouseEventArgs e)
        {
            MySQL.VisiIND(vt_dgv.CurrentCell.RowIndex);
            string firstCellValue = vt_dgv.SelectedRows[0].Cells[0].Value.ToString();
            string query = "SELECT * FROM visiting_tutor WHERE vt_id=" + firstCellValue + ";";
            MySQL.tempVTStore(query);
            Asterisk_EditVisitingTutors frm = new Asterisk_EditVisitingTutors();
            frm.Show();
        }
        /*
        private void button1_Click_1(object sender, EventArgs e)
        {
            
            string query = "SELECT DATE_FORMAT(deadline, '%d/%m/%Y') AS deadline From role WHERE r_id = 1";
            MySQL.tempRoleStore(query);
            MessageBox.Show(EditTempRoles.deadline);
            
        }
        */
    }
}
