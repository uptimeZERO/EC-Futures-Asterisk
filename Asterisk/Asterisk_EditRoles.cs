﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Asterisk
{
    public partial class Asterisk_EditRoles : Form
    {
        public Asterisk_EditRoles()
        {
            InitializeComponent();
        }
        DBConnect MySQL = new DBConnect();
        private void Asterisk_EditRoles_Load(object sender, EventArgs e)
        {
            string dDD = EditTempRoles.deadline.Substring(0, 2);
            string dMM = EditTempRoles.deadline.Substring(3, 2);
            string dYYYY = EditTempRoles.deadline.Substring(6, 4);
            string sDD = EditTempRoles.start_date.Substring(0, 2);
            string sMM = EditTempRoles.start_date.Substring(3, 2);
            string sYYYY = EditTempRoles.start_date.Substring(6, 4);
            string eDD = EditTempRoles.end_date.Substring(0, 2);
            string eMM = EditTempRoles.end_date.Substring(3, 2);
            string eYYYY = EditTempRoles.end_date.Substring(6, 4);
            try
            {
                string SelectQuery = "SELECT * FROM employer;";
                MySQL.tempDS1(SelectQuery);
                employer_cmb.DisplayMember = "name";
                employer_cmb.ValueMember = "e_id";
                employer_cmb.DataSource = MySQL.dt1;

            }
            catch
            {
                MessageBox.Show("An error has occured, please check your connection", "EC Futures");
            }
            try
            {
                string SelectQuery = "SELECT c_id, e_id, CONCAT(forename, ' ', surname) AS Full FROM contact WHERE e_id=" + EditTempRoles.e_id + ";";
                MySQL.tempDS2(SelectQuery);
                contact_cmb.DisplayMember = "Full";
                contact_cmb.ValueMember = "c_id";
                contact_cmb.DataSource = MySQL.dt2;

            }
            catch
            {
                MessageBox.Show("An error has occured, please check your connection", "EC Futures");
            }
            title_txt.Text = EditTempRoles.title;
            salary_txt.Text = EditTempRoles.salary;
            employer_cmb.SelectedValue = EditTempRoles.e_id;
            contact_cmb.SelectedValue = EditTempRoles.c_id;
            int idYYYY;
            bool dY_parsed = Int32.TryParse(dYYYY, out idYYYY);
            int idMM;
            bool dM_parsed = Int32.TryParse(dMM, out idMM);
            int idDD;
            bool dD_parsed = Int32.TryParse(dDD, out idDD);
            deadline_dtp.Format = DateTimePickerFormat.Custom;
            deadline_dtp.CustomFormat = "dd-MM-yyyy";
            //MessageBox.Show(EditTempRoles.deadline);
            deadline_dtp.Value = new DateTime(idYYYY, idMM, idDD);
            int isYYYY;
            bool sY_parsed = Int32.TryParse(sYYYY, out isYYYY);
            int isMM;
            bool sM_parsed = Int32.TryParse(sMM, out isMM);
            int isDD;
            bool sD_parsed = Int32.TryParse(sDD, out isDD);
            start_dtp.Format = DateTimePickerFormat.Custom;
            start_dtp.CustomFormat = "dd-MM-yyyy";
            start_dtp.Value = new DateTime(isYYYY, isMM, isDD);
            int ieYYYY;
            bool eY_parsed = Int32.TryParse(eYYYY, out ieYYYY);
            int ieMM;
            bool eM_parsed = Int32.TryParse(eMM, out ieMM);
            int ieDD;
            bool eD_parsed = Int32.TryParse(eDD, out ieDD);
            
            end_dtp.Format = DateTimePickerFormat.Custom;
            end_dtp.CustomFormat = "dd-MM-yyyy";
            end_dtp.Value = new DateTime(ieYYYY, ieMM, ieDD);
            notes_txt.Text = EditTempRoles.notes;
        }

        private void cancel_btn_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void add_btn_Click(object sender, EventArgs e)
        {
            if (title_txt.Text == "" || salary_txt.Text == "" || contact_cmb.Text == "")
            {
                MessageBox.Show("Please make sure you have entered the appropriate data in the required(*) fields", "EC Futures");
            }
            else
            {
                try
                {
                    string e_str = employer_cmb.SelectedValue.ToString();
                    int e_id;
                    bool e_parsed = Int32.TryParse(e_str, out e_id);
                    string c_str = contact_cmb.SelectedValue.ToString();
                    int c_id;
                    bool c_parsed = Int32.TryParse(c_str, out c_id);
                    string deadline = deadline_dtp.Value.ToString("yyyy-MM-dd HH':'mm':'ss");
                    string startDate = start_dtp.Value.ToString("yyyy-MM-dd HH':'mm':'ss");
                    string endDate = end_dtp.Value.ToString("yyyy-MM-dd HH':'mm':'ss");
                    int startIndex = 0;
                    int length = 10;
                    string dead = deadline.Substring(startIndex, length);
                    string start = startDate.Substring(startIndex, length);
                    string end = endDate.Substring(startIndex, length);
                    string query = "UPDATE role SET title='" + title_txt.Text + "', salary='" + salary_txt.Text + "', e_id=" + e_id + ", c_id=" + c_id + ", deadline='" + dead + "', start_date='" + start + "', end_date='" + end + "', notes='" + notes_txt.Text + "' WHERE r_id=" + EditTempRoles.r_id + ";";
                    MySQL.PerformQuery(query);
                    MySQL.RoleREF(1);
                    this.Hide();
                }
                catch
                {
                    MessageBox.Show("An error has occured, please check your connection", "EC Futures");
                }

            }
        }

        private void employer_cmb_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string f_str = employer_cmb.SelectedValue.ToString();
                string SelectQuery = "SELECT c_id, e_id, CONCAT(forename, ' ', surname) AS Full FROM contact WHERE e_id=" + f_str + ";";
                MySQL.tempDS2(SelectQuery);
                contact_cmb.DisplayMember = "Full";
                contact_cmb.ValueMember = "c_id";
                contact_cmb.DataSource = MySQL.dt2;
            }
            catch
            {
                MessageBox.Show("An error has occured, please check your connection", "EC Futures");
            }
        }
    }
}
