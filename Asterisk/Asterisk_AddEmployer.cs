﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Asterisk
{
    public partial class Asterisk_AddEmployer : Form
    {
        public Asterisk_AddEmployer()
        {
            InitializeComponent();
        }

        private void cancel_btn_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void add_btn_Click(object sender, EventArgs e)
        {
            DBConnect MySQL = new DBConnect();
            if (employer_txt.Text == "" || address1_txt.Text == "" || settlement_txt.Text == "" || county_txt.Text == "" || postcode_txt.Text == "" || country_cmb.Text == "")
            {
                MessageBox.Show("Please make sure you have entered the appropriate data in the required(*) fields", "EC Futures");
            }
            else
            {
                try
                {
                    string query = "INSERT INTO employer (`e_id`, `name`, `address_1`, `address_2`, `settlement`, `county`, `post_code`, `country`, `notes`) VALUES (NULL, '" + employer_txt.Text + "', '" + address1_txt.Text + "', '" + address2_txt.Text + "', '" + settlement_txt.Text + "', '" + county_txt.Text + "', '" + postcode_txt.Text + "', '" + country_cmb.Text.ToString() + "', '" + notes_txt.Text + "');";
                    MySQL.PerformQuery(query);
                    MySQL.EmpREF(1);
                    this.Hide();
                }
                catch
                {
                    MessageBox.Show("An error has occured, please check your connection", "EC Futures");
                }

            }
        }
    }
}
