﻿namespace Asterisk
{
    partial class Asterisk_CreateNewUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Asterisk_CreateNewUser));
            this.label1 = new System.Windows.Forms.Label();
            this.newUser_Txt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.newPass_Txt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.conPass_Txt = new System.Windows.Forms.TextBox();
            this.Create_User_btn = new System.Windows.Forms.Button();
            this.Cancel_btn = new System.Windows.Forms.Button();
            this.Usr_Grp_bx = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.newForename_Txt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.newSurname_Txt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Username*";
            // 
            // newUser_Txt
            // 
            this.newUser_Txt.Location = new System.Drawing.Point(13, 110);
            this.newUser_Txt.Name = "newUser_Txt";
            this.newUser_Txt.Size = new System.Drawing.Size(209, 20);
            this.newUser_Txt.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(234, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Password*";
            // 
            // newPass_Txt
            // 
            this.newPass_Txt.Location = new System.Drawing.Point(237, 28);
            this.newPass_Txt.Name = "newPass_Txt";
            this.newPass_Txt.PasswordChar = '*';
            this.newPass_Txt.Size = new System.Drawing.Size(209, 20);
            this.newPass_Txt.TabIndex = 3;
            this.newPass_Txt.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(234, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Confirm Password*";
            // 
            // conPass_Txt
            // 
            this.conPass_Txt.Location = new System.Drawing.Point(237, 68);
            this.conPass_Txt.Name = "conPass_Txt";
            this.conPass_Txt.PasswordChar = '*';
            this.conPass_Txt.Size = new System.Drawing.Size(209, 20);
            this.conPass_Txt.TabIndex = 4;
            // 
            // Create_User_btn
            // 
            this.Create_User_btn.Location = new System.Drawing.Point(371, 137);
            this.Create_User_btn.Name = "Create_User_btn";
            this.Create_User_btn.Size = new System.Drawing.Size(75, 23);
            this.Create_User_btn.TabIndex = 6;
            this.Create_User_btn.Text = "Create User";
            this.Create_User_btn.UseVisualStyleBackColor = true;
            this.Create_User_btn.Click += new System.EventHandler(this.Create_User_btn_Click);
            // 
            // Cancel_btn
            // 
            this.Cancel_btn.Location = new System.Drawing.Point(290, 137);
            this.Cancel_btn.Name = "Cancel_btn";
            this.Cancel_btn.Size = new System.Drawing.Size(75, 23);
            this.Cancel_btn.TabIndex = 7;
            this.Cancel_btn.Text = "Cancel";
            this.Cancel_btn.UseVisualStyleBackColor = true;
            this.Cancel_btn.Click += new System.EventHandler(this.Cancel_btn_Click);
            // 
            // Usr_Grp_bx
            // 
            this.Usr_Grp_bx.FormattingEnabled = true;
            this.Usr_Grp_bx.Items.AddRange(new object[] {
            "Standard",
            "Administrator"});
            this.Usr_Grp_bx.Location = new System.Drawing.Point(237, 109);
            this.Usr_Grp_bx.Name = "Usr_Grp_bx";
            this.Usr_Grp_bx.Size = new System.Drawing.Size(209, 21);
            this.Usr_Grp_bx.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(234, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "User Group*";
            // 
            // newForename_Txt
            // 
            this.newForename_Txt.Location = new System.Drawing.Point(13, 28);
            this.newForename_Txt.Name = "newForename_Txt";
            this.newForename_Txt.Size = new System.Drawing.Size(209, 20);
            this.newForename_Txt.TabIndex = 0;
            this.newForename_Txt.TextChanged += new System.EventHandler(this.newForename_Txt_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "First Name*";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // newSurname_Txt
            // 
            this.newSurname_Txt.Location = new System.Drawing.Point(13, 68);
            this.newSurname_Txt.Name = "newSurname_Txt";
            this.newSurname_Txt.Size = new System.Drawing.Size(209, 20);
            this.newSurname_Txt.TabIndex = 1;
            this.newSurname_Txt.TextChanged += new System.EventHandler(this.textBox2_TextChanged_1);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 52);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 21;
            this.label6.Text = "Last Name*";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 142);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 13);
            this.label7.TabIndex = 35;
            this.label7.Text = "* is a required field";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // Create_new_user
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 168);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.newSurname_Txt);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.newForename_Txt);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Usr_Grp_bx);
            this.Controls.Add(this.Cancel_btn);
            this.Controls.Add(this.Create_User_btn);
            this.Controls.Add(this.conPass_Txt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.newPass_Txt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.newUser_Txt);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Create_new_user";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add New User";
            this.Load += new System.EventHandler(this.Create_new_user_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox newUser_Txt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox newPass_Txt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox conPass_Txt;
        private System.Windows.Forms.Button Create_User_btn;
        private System.Windows.Forms.Button Cancel_btn;
        private System.Windows.Forms.ComboBox Usr_Grp_bx;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox newForename_Txt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox newSurname_Txt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}