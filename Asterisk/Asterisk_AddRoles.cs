﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Asterisk
{
    public partial class Asterisk_AddRoles : Form
    {
        public Asterisk_AddRoles()
        {
            InitializeComponent();
        }
        DBConnect MySQL = new DBConnect();
        private void Asterisk_AddRoles_Load(object sender, EventArgs e)
        {
            try
            {
                string SelectQuery = "SELECT * FROM employer;";
                MySQL.tempDS1(SelectQuery);
                employer_cmb.DisplayMember = "name";
                employer_cmb.ValueMember = "e_id";
                employer_cmb.DataSource = MySQL.dt1;

            }
            catch
            {
                MessageBox.Show("An error has occured, please check your connection", "EC Futures");
            }
        }
        string f_str;
        private void employer_cmb_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                f_str = employer_cmb.SelectedValue.ToString();
                string SelectQuery = "SELECT c_id, e_id, CONCAT(forename, ' ', surname) AS Full FROM contact WHERE e_id=" + f_str + ";";
                MySQL.tempDS2(SelectQuery);
                contact_cmb.DisplayMember = "Full";
                contact_cmb.ValueMember = "c_id";
                contact_cmb.DataSource = MySQL.dt2;  
                         
            }
            catch
            {
                MessageBox.Show("An error has occured, please check your connection", "EC Futures");
            }
        }

        private void cancel_btn_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void contact_cmb_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void add_btn_Click(object sender, EventArgs e)
        {
            if (title_txt.Text == "" || salary_txt.Text == "")
            {
                MessageBox.Show("Please make sure you have entered the appropriate data in the required(*) fields", "EC Futures");
            }
            else
            {
                try
                {
                    string e_str = employer_cmb.SelectedValue.ToString();
                    int e_id;
                    bool e_parsed = Int32.TryParse(e_str, out e_id);
                    string c_str = contact_cmb.SelectedValue.ToString();
                    int c_id;
                    bool c_parsed = Int32.TryParse(c_str, out c_id);
                    string deadline = deadline_dtp.Value.ToString("yyyy-MM-dd HH':'mm':'ss");
                    string startDate = start_dtp.Value.ToString("yyyy-MM-dd HH':'mm':'ss");
                    string endDate = end_dtp.Value.ToString("yyyy-MM-dd HH':'mm':'ss");
                    int startIndex = 0;
                    int length = 10;
                    string dead = deadline.Substring(startIndex, length);
                    string start = startDate.Substring(startIndex, length);
                    string end = endDate.Substring(startIndex, length);
                    string query = "INSERT INTO role (`r_id`, `title`, `salary`, `e_id`, `c_id`, `deadline`, `start_date`, `end_date`, `notes`) VALUES (NULL, '" + title_txt.Text + "', '" + salary_txt.Text + "', " + e_id + ", " + c_id + ", '" + dead + "', '" + start + "', '" + end + "', '" + notes_txt.Text + "');";
                    MySQL.PerformQuery(query);
                    MySQL.RoleREF(1);
                    this.Hide();
                }
                catch
                {
                    MessageBox.Show("An error has occured, please check your connection", "EC Futures");
                }

            }
            
        }
    }
}
