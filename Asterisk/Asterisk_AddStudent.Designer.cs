﻿namespace Asterisk
{
    partial class Asterisk_AddStudent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Asterisk_AddStudent));
            this.label1 = new System.Windows.Forms.Label();
            this.cancel_btn = new System.Windows.Forms.Button();
            this.add_btn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.studentid_txt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.forename_txt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.surname_txt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.faculty_cmb = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.course_cmb = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.email_txt = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.phone_txt = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.upload_btn = new System.Windows.Forms.Button();
            this.view_btn = new System.Windows.Forms.Button();
            this.cv_stat_txt = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.notes_txt = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Student ID*";
            // 
            // cancel_btn
            // 
            this.cancel_btn.Location = new System.Drawing.Point(292, 209);
            this.cancel_btn.Name = "cancel_btn";
            this.cancel_btn.Size = new System.Drawing.Size(75, 23);
            this.cancel_btn.TabIndex = 11;
            this.cancel_btn.Text = "Cancel";
            this.cancel_btn.UseVisualStyleBackColor = true;
            this.cancel_btn.Click += new System.EventHandler(this.cancel_btn_Click);
            // 
            // add_btn
            // 
            this.add_btn.Location = new System.Drawing.Point(373, 209);
            this.add_btn.Name = "add_btn";
            this.add_btn.Size = new System.Drawing.Size(75, 23);
            this.add_btn.TabIndex = 10;
            this.add_btn.Text = "Add";
            this.add_btn.UseVisualStyleBackColor = true;
            this.add_btn.Click += new System.EventHandler(this.add_btn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 216);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "* is a required field";
            // 
            // studentid_txt
            // 
            this.studentid_txt.Location = new System.Drawing.Point(15, 25);
            this.studentid_txt.Name = "studentid_txt";
            this.studentid_txt.Size = new System.Drawing.Size(209, 20);
            this.studentid_txt.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Forename*";
            // 
            // forename_txt
            // 
            this.forename_txt.Location = new System.Drawing.Point(15, 64);
            this.forename_txt.Name = "forename_txt";
            this.forename_txt.Size = new System.Drawing.Size(100, 20);
            this.forename_txt.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(121, 48);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Surname*";
            // 
            // surname_txt
            // 
            this.surname_txt.Location = new System.Drawing.Point(124, 64);
            this.surname_txt.Name = "surname_txt";
            this.surname_txt.Size = new System.Drawing.Size(100, 20);
            this.surname_txt.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 87);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 13);
            this.label5.TabIndex = 17;
            this.label5.Text = "Faculty*";
            // 
            // faculty_cmb
            // 
            this.faculty_cmb.FormattingEnabled = true;
            this.faculty_cmb.Location = new System.Drawing.Point(15, 103);
            this.faculty_cmb.Name = "faculty_cmb";
            this.faculty_cmb.Size = new System.Drawing.Size(209, 21);
            this.faculty_cmb.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 127);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Degree*";
            // 
            // course_cmb
            // 
            this.course_cmb.FormattingEnabled = true;
            this.course_cmb.Location = new System.Drawing.Point(15, 143);
            this.course_cmb.Name = "course_cmb";
            this.course_cmb.Size = new System.Drawing.Size(209, 21);
            this.course_cmb.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 167);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 13);
            this.label7.TabIndex = 21;
            this.label7.Text = "Email*";
            // 
            // email_txt
            // 
            this.email_txt.Location = new System.Drawing.Point(15, 183);
            this.email_txt.Name = "email_txt";
            this.email_txt.Size = new System.Drawing.Size(209, 20);
            this.email_txt.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(236, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 23;
            this.label8.Text = "Phone*";
            // 
            // phone_txt
            // 
            this.phone_txt.Location = new System.Drawing.Point(239, 25);
            this.phone_txt.Name = "phone_txt";
            this.phone_txt.Size = new System.Drawing.Size(209, 20);
            this.phone_txt.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(239, 48);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 13);
            this.label9.TabIndex = 25;
            this.label9.Text = "CV";
            // 
            // upload_btn
            // 
            this.upload_btn.Location = new System.Drawing.Point(373, 64);
            this.upload_btn.Name = "upload_btn";
            this.upload_btn.Size = new System.Drawing.Size(75, 20);
            this.upload_btn.TabIndex = 8;
            this.upload_btn.Text = "Upload";
            this.upload_btn.UseVisualStyleBackColor = true;
            // 
            // view_btn
            // 
            this.view_btn.Location = new System.Drawing.Point(292, 64);
            this.view_btn.Name = "view_btn";
            this.view_btn.Size = new System.Drawing.Size(75, 20);
            this.view_btn.TabIndex = 7;
            this.view_btn.Text = "View";
            this.view_btn.UseVisualStyleBackColor = true;
            // 
            // cv_stat_txt
            // 
            this.cv_stat_txt.Location = new System.Drawing.Point(239, 64);
            this.cv_stat_txt.Name = "cv_stat_txt";
            this.cv_stat_txt.ReadOnly = true;
            this.cv_stat_txt.Size = new System.Drawing.Size(47, 20);
            this.cv_stat_txt.TabIndex = 28;
            this.cv_stat_txt.Text = "No CV";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(236, 87);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 13);
            this.label10.TabIndex = 29;
            this.label10.Text = "Notes";
            // 
            // notes_txt
            // 
            this.notes_txt.Location = new System.Drawing.Point(239, 103);
            this.notes_txt.Multiline = true;
            this.notes_txt.Name = "notes_txt";
            this.notes_txt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.notes_txt.Size = new System.Drawing.Size(209, 100);
            this.notes_txt.TabIndex = 9;
            // 
            // Asterisk_AddStudent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 241);
            this.Controls.Add(this.notes_txt);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.cv_stat_txt);
            this.Controls.Add(this.view_btn);
            this.Controls.Add(this.upload_btn);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.phone_txt);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.email_txt);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.course_cmb);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.faculty_cmb);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.surname_txt);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.forename_txt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.studentid_txt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.add_btn);
            this.Controls.Add(this.cancel_btn);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Asterisk_AddStudent";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add Student";
            this.Load += new System.EventHandler(this.Asterisk_AddStudent_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cancel_btn;
        private System.Windows.Forms.Button add_btn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox studentid_txt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox forename_txt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox surname_txt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox faculty_cmb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox course_cmb;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox email_txt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox phone_txt;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button upload_btn;
        private System.Windows.Forms.Button view_btn;
        private System.Windows.Forms.TextBox cv_stat_txt;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox notes_txt;
    }
}